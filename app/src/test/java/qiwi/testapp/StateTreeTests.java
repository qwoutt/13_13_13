package qiwi.testapp;


import org.junit.Test;

import java.util.HashSet;

import io.reactivex.subjects.PublishSubject;
import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.shemamodel.Predicate;
import qiwi.testapp.builder.shemamodel.Validator;
import qiwi.testapp.builder.ui.uimodel.UIField;
import qiwi.testapp.builder.ui.uimodel.UIValidator;

public class StateTreeTests {

    static class UIFieldTestImp extends UIField{
        int testID = 0;


        public UIFieldTestImp(int testID) {
            element = new Element();
            this.testID = testID;
        }

        public void setTestID(int testID) {
            this.testID = testID;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            UIFieldTestImp that = (UIFieldTestImp) o;

            return testID == that.testID;
        }

        @Override
        public int hashCode() {
           return testID;
        }
    }

    private Validator bikValidator(){
        Validator validator = new Validator();
        validator.type      = "predicate";
        validator.message   = "Неверный БИК";
        Predicate predicate = new Predicate();
        predicate.pattern   = "^\\d{9}$";
        predicate.type      = "regex";
        validator.predicate = predicate;
        return validator;
    }

    private Validator cardValidator(){
        Validator validator = new Validator();
        validator.type      = "predicate";
        validator.message   = "Неверный номер карты";
        Predicate predicate = new Predicate();
        predicate.pattern   = "^\\d{4} \\d{4} \\d{4} \\d{4,7}$";
        predicate.type      = "regex";
        validator.predicate = predicate;
        return validator;
    }

    @Test
    public void UIRxTest() throws InterruptedException {

        PublishSubject<String> oMock1 = PublishSubject.create();
        PublishSubject<String> oMock2 = PublishSubject.create();


        UIFieldTestImp  fieldTestImp1 = new UIFieldTestImp(1);
        UIFieldTestImp  fieldTestImp2 = new UIFieldTestImp(2);

        fieldTestImp1.element.validator = bikValidator();
        fieldTestImp2.element.validator = cardValidator();

        UIValidator validator1 = new UIValidator(fieldTestImp1, oMock1);
        UIValidator validator2 = new UIValidator(fieldTestImp2, oMock2);

        HashSet<UIValidator> validators = new HashSet<>(2);
        validators.add(validator1);
        validators.add(validator2);

        InputStateTree tree = new InputStateTree(validators, 100);

        tree.bind();


        tree.getStateObserver()
                .subscribe(next -> {
                    //if(next instanceof ViewState.InputState.ShowError){
                    //    System.out.println(((ViewState.InputState.ShowError) next).getMessage());
                    //}else{
                    System.out.println(next.getClass());
                    //}
                });



        //  oMock1.onNext("2");
        //oMock1.onNext("5");
        //oMock1.onNext("2");
        //oMock1.onNext("2");
        //Thread.sleep(1000);
        //oMock1.onNext("2");
        //oMock1.onNext("2");

        Thread.sleep(1000);

    }

}
