package qiwi.testapp;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Test;

import java.util.Map;

import qiwi.testapp.builder.shemamodel.Content;
import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.shemamodel.UIFormat;
import qiwi.testapp.builder.viewmodel.JsonDelegate;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {


   public static class Payload{
        static String json = "{\n" +
                "  \"content\":{\n" +
                "    \"elements\":[\n" +
                "      {\n" +
                "        \"type\":\"field\",\n" +
                "        \"name\":\"account_type\",\n" +
                "        \"validator\":{\n" +
                "          \"type\":\"predicate\",\n" +
                "          \"predicate\":{\n" +
                "            \"type\":\"regex\",\n" +
                "            \"pattern\":\"^2$|^5$\"\n" +
                "          },\n" +
                "          \"message\":\"Необходимо выбрать тип идентификатора\"\n" +
                "        },\n" +
                "        \"view\":{\n" +
                "          \"title\":\"Тип идентификатора\",\n" +
                "          \"prompt\":\"Выберите тип идентификатора\",\n" +
                "          \"widget\":{\n" +
                "            \"type\":\"radio\",\n" +
                "            \"choices\":[\n" +
                "              {\n" +
                "                \"value\":\"2\",\n" +
                "                \"title\":\"Номер счета\"\n" +
                "              },\n" +
                "              {\n" +
                "                \"value\":\"5\",\n" +
                "                \"title\":\"Номер карты\"\n" +
                "              }\n" +
                "            ]\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"type\":\"dependency\",\n" +
                "        \"condition\":{\n" +
                "          \"type\":\"predicate\",\n" +
                "          \"field\":\"account_type\",\n" +
                "          \"predicate\":{\n" +
                "            \"type\":\"regex\",\n" +
                "            \"pattern\":\"^2$\"\n" +
                "          }\n" +
                "        },\n" +
                "        \"content\":{\n" +
                "          \"elements\":[\n" +
                "            {\n" +
                "              \"type\":\"field\",\n" +
                "              \"name\":\"mfo\",\n" +
                "              \"validator\":{\n" +
                "                \"type\":\"predicate\",\n" +
                "                \"predicate\":{\n" +
                "                  \"type\":\"regex\",\n" +
                "                  \"pattern\":\"^\\\\d{9}$\"\n" +
                "                },\n" +
                "                \"message\":\"Неверный БИК\"\n" +
                "              },\n" +
                "              \"view\":{\n" +
                "                \"title\":\"БИК\",\n" +
                "                \"prompt\":\"Введите БИК\",\n" +
                "                \"widget\":{\n" +
                "                  \"type\":\"text\",\n" +
                "                  \"keyboard\":\"numeric\"\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            {\n" +
                "              \"type\":\"field\",\n" +
                "              \"name\":\"account\",\n" +
                "              \"validator\":{\n" +
                "                \"type\":\"predicate\",\n" +
                "                \"predicate\":{\n" +
                "                  \"type\":\"regex\",\n" +
                "                  \"pattern\":\"^\\\\d{20}$\"\n" +
                "                },\n" +
                "                \"message\":\"Неверное значение\"\n" +
                "              },\n" +
                "              \"view\":{\n" +
                "                \"title\":\"Номер счета\",\n" +
                "                \"prompt\":\"Номер счета\",\n" +
                "                \"widget\":{\n" +
                "                  \"type\":\"text\",\n" +
                "                  \"keyboard\":\"numeric\"\n" +
                "                }\n" +
                "              }\n" +
                "            }\n" +
                "          ]\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"type\":\"dependency\",\n" +
                "        \"condition\":{\n" +
                "          \"type\":\"predicate\",\n" +
                "          \"field\":\"account_type\",\n" +
                "          \"predicate\":{\n" +
                "            \"type\":\"regex\",\n" +
                "            \"pattern\":\"^5$\"\n" +
                "          }\n" +
                "        },\n" +
                "        \"content\":{\n" +
                "          \"elements\":[\n" +
                "            {\n" +
                "              \"type\":\"field\",\n" +
                "              \"name\":\"account\",\n" +
                "              \"semantics\":{\n" +
                "                \"type\":\"CardNumber\"\n" +
                "              },\n" +
                "              \"validator\":{\n" +
                "                \"type\":\"predicate\",\n" +
                "                \"predicate\":{\n" +
                "                  \"type\":\"regex\",\n" +
                "                  \"pattern\":\"^\\\\d{4} \\\\d{4} \\\\d{4} \\\\d{4,7}$\"\n" +
                "                },\n" +
                "                \"message\":\"Неверный номер карты\"\n" +
                "              },\n" +
                "              \"view\":{\n" +
                "                \"title\":\"Номер карты\",\n" +
                "                \"prompt\":\"Введите номер карты\",\n" +
                "                \"widget\":{\n" +
                "                  \"type\":\"text\"\n" +
                "                }\n" +
                "              }\n" +
                "            }\n" +
                "          ]\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"type\":\"dependency\",\n" +
                "        \"condition\":{\n" +
                "          \"type\":\"predicate\",\n" +
                "          \"field\":\"account_type\",\n" +
                "          \"predicate\":{\n" +
                "            \"type\":\"regex\",\n" +
                "            \"pattern\":\"^2$\"\n" +
                "          }\n" +
                "        },\n" +
                "        \"content\":{\n" +
                "          \"elements\":[\n" +
                "            {\n" +
                "              \"type\":\"field\",\n" +
                "              \"name\":\"urgent\",\n" +
                "              \"validator\":{\n" +
                "                \"type\":\"predicate\",\n" +
                "                \"predicate\":{\n" +
                "                  \"type\":\"regex\",\n" +
                "                  \"pattern\":\"^0$|^1$\"\n" +
                "                },\n" +
                "                \"message\":\"Необходимо выбрать тип платежа\"\n" +
                "              },\n" +
                "              \"view\":{\n" +
                "                \"title\":\"Тип платежа\",\n" +
                "                \"prompt\":\"Выберите тип платежа\",\n" +
                "                \"widget\":{\n" +
                "                  \"type\":\"radio\",\n" +
                "                  \"choices\":[\n" +
                "                    {\n" +
                "                      \"value\":\"0\",\n" +
                "                      \"title\":\"Обычный платеж\"\n" +
                "                    },\n" +
                "                    {\n" +
                "                      \"value\":\"1\",\n" +
                "                      \"title\":\"Срочный платеж\"\n" +
                "                    }\n" +
                "                  ]\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            {\n" +
                "              \"type\":\"field\",\n" +
                "              \"name\":\"lname\",\n" +
                "              \"semantics\":{\n" +
                "                \"type\":\"LastName\"\n" +
                "              },\n" +
                "              \"validator\":{\n" +
                "                \"type\":\"predicate\",\n" +
                "                \"predicate\":{\n" +
                "                  \"type\":\"regex\",\n" +
                "                  \"pattern\":\"^[а-яА-Я\\\\-\\\\s]{2,40}$\"\n" +
                "                },\n" +
                "                \"message\":\"Неверное значение\"\n" +
                "              },\n" +
                "              \"view\":{\n" +
                "                \"title\":\"Фамилия владельца счета\",\n" +
                "                \"prompt\":\"Фамилия владельца счета\",\n" +
                "                \"widget\":{\n" +
                "                  \"type\":\"text\"\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            {\n" +
                "              \"type\":\"field\",\n" +
                "              \"name\":\"fname\",\n" +
                "              \"semantics\":{\n" +
                "                \"type\":\"FirstName\"\n" +
                "              },\n" +
                "              \"validator\":{\n" +
                "                \"type\":\"predicate\",\n" +
                "                \"predicate\":{\n" +
                "                  \"type\":\"regex\",\n" +
                "                  \"pattern\":\"^[а-яА-Я\\\\-\\\\s]{2,40}$\"\n" +
                "                },\n" +
                "                \"message\":\"Неверное значение\"\n" +
                "              },\n" +
                "              \"view\":{\n" +
                "                \"title\":\"Имя владельца счета\",\n" +
                "                \"prompt\":\"Имя владельца счета\",\n" +
                "                \"widget\":{\n" +
                "                  \"type\":\"text\"\n" +
                "                }\n" +
                "              }\n" +
                "            },\n" +
                "            {\n" +
                "              \"type\":\"field\",\n" +
                "              \"name\":\"mname\",\n" +
                "              \"semantics\":{\n" +
                "                \"type\":\"MiddleName\"\n" +
                "              },\n" +
                "              \"validator\":{\n" +
                "                \"type\":\"predicate\",\n" +
                "                \"predicate\":{\n" +
                "                  \"type\":\"regex\",\n" +
                "                  \"pattern\":\"^[а-яА-Я\\\\-\\\\s]{2,40}$\"\n" +
                "                },\n" +
                "                \"message\":\"Неверное значение\"\n" +
                "              },\n" +
                "              \"view\":{\n" +
                "                \"title\":\"Отчество владельца счета\",\n" +
                "                \"prompt\":\"Отчество владельца счета\",\n" +
                "                \"widget\":{\n" +
                "                  \"type\":\"text\"\n" +
                "                }\n" +
                "              }\n" +
                "            }\n" +
                "          ]\n" +
                "        }\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}\n";

        public static String fromFile = "{  \"content\":{    \"elements\":[      {        \"type\":\"field\",        \"name\":\"account_type\",        \"validator\":{          \"type\":\"predicate\",          \"predicate\":{            \"type\":\"regex\",            \"pattern\":\"^2$|^5$\"          },          \"message\":\"Необходимо выбрать тип идентификатора\"        },        \"view\":{          \"title\":\"Тип идентификатора\",          \"prompt\":\"Выберите тип идентификатора\",          \"widget\":{            \"type\":\"radio\",            \"choices\":[              {                \"value\":\"2\",                \"title\":\"Номер счета\"              },              {                \"value\":\"5\",                \"title\":\"Номер карты\"              }            ]          }        }      },      {        \"type\":\"dependency\",        \"condition\":{          \"type\":\"predicate\",          \"field\":\"account_type\",          \"predicate\":{            \"type\":\"regex\",            \"pattern\":\"^2$\"          }        },        \"content\":{          \"elements\":[            {              \"type\":\"field\",              \"name\":\"mfo\",              \"validator\":{                \"type\":\"predicate\",                \"predicate\":{                  \"type\":\"regex\",                  \"pattern\":\"^\\\\d{9}$\"                },                \"message\":\"Неверный БИК\"              },              \"view\":{                \"title\":\"БИК\",                \"prompt\":\"Введите БИК\",                \"widget\":{                  \"type\":\"text\",                  \"keyboard\":\"numeric\"                }              }            },            {              \"type\":\"field\",              \"name\":\"account\",              \"validator\":{                \"type\":\"predicate\",                \"predicate\":{                  \"type\":\"regex\",                  \"pattern\":\"^\\\\d{20}$\"                },                \"message\":\"Неверное значение\"              },              \"view\":{                \"title\":\"Номер счета\",                \"prompt\":\"Номер счета\",                \"widget\":{                  \"type\":\"text\",                  \"keyboard\":\"numeric\"                }              }            }          ]        }      },      {        \"type\":\"dependency\",        \"condition\":{          \"type\":\"predicate\",          \"field\":\"account_type\",          \"predicate\":{            \"type\":\"regex\",            \"pattern\":\"^5$\"          }        },        \"content\":{          \"elements\":[            {              \"type\":\"field\",              \"name\":\"account\",              \"semantics\":{                \"type\":\"CardNumber\"              },              \"validator\":{                \"type\":\"predicate\",                \"predicate\":{                  \"type\":\"regex\",                  \"pattern\":\"^\\\\d{4} \\\\d{4} \\\\d{4} \\\\d{4,7}$\"                },                \"message\":\"Неверный номер карты\"              },              \"view\":{                \"title\":\"Номер карты\",                \"prompt\":\"Введите номер карты\",                \"widget\":{                  \"type\":\"text\"                }              }            }          ]        }      },      {        \"type\":\"dependency\",        \"condition\":{          \"type\":\"predicate\",          \"field\":\"account_type\",          \"predicate\":{            \"type\":\"regex\",            \"pattern\":\"^2$\"          }        },        \"content\":{          \"elements\":[            {              \"type\":\"field\",              \"name\":\"urgent\",              \"validator\":{                \"type\":\"predicate\",                \"predicate\":{                  \"type\":\"regex\",                  \"pattern\":\"^0$|^1$\"                },                \"message\":\"Необходимо выбрать тип платежа\"              },              \"view\":{                \"title\":\"Тип платежа\",                \"prompt\":\"Выберите тип платежа\",                \"widget\":{                  \"type\":\"radio\",                  \"choices\":[                    {                      \"value\":\"0\",                      \"title\":\"Обычный платеж\"                    },                    {                      \"value\":\"1\",                      \"title\":\"Срочный платеж\"                    }                  ]                }              }            },            {              \"type\":\"field\",              \"name\":\"lname\",              \"semantics\":{                \"type\":\"LastName\"              },              \"validator\":{                \"type\":\"predicate\",                \"predicate\":{                  \"type\":\"regex\",                  \"pattern\":\"^[а-яА-Я\\\\-\\\\s]{2,40}$\"                },                \"message\":\"Неверное значение\"              },              \"view\":{                \"title\":\"Фамилия владельца счета\",                \"prompt\":\"Фамилия владельца счета\",                \"widget\":{                  \"type\":\"text\"                }              }            },            {              \"type\":\"field\",              \"name\":\"fname\",              \"semantics\":{                \"type\":\"FirstName\"              },              \"validator\":{                \"type\":\"predicate\",                \"predicate\":{                  \"type\":\"regex\",                  \"pattern\":\"^[а-яА-Я\\\\-\\\\s]{2,40}$\"                },                \"message\":\"Неверное значение\"              },              \"view\":{                \"title\":\"Имя владельца счета\",                \"prompt\":\"Имя владельца счета\",                \"widget\":{                  \"type\":\"text\"                }              }            },            {              \"type\":\"field\",              \"name\":\"mname\",              \"semantics\":{                \"type\":\"MiddleName\"              },              \"validator\":{                \"type\":\"predicate\",                \"predicate\":{                  \"type\":\"regex\",                  \"pattern\":\"^[а-яА-Я\\\\-\\\\s]{2,40}$\"                },                \"message\":\"Неверное значение\"              },              \"view\":{                \"title\":\"Отчество владельца счета\",                \"prompt\":\"Отчество владельца счета\",                \"widget\":{                  \"type\":\"text\"                }              }            }          ]        }      }    ]  }}";
    }

    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }


    @Test
    public void testFullGson(){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Content test = gson.fromJson(Payload.json, Content.class);

        System.out.println(test);
    }

    @Test
    public void testJsonFromFile(){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        UIFormat test = gson.fromJson(Payload.fromFile, UIFormat.class);

        System.out.println(test);
    }

}