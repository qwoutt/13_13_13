package qiwi.testapp;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;
import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.shemamodel.UIFormat;
import qiwi.testapp.builder.ui.UIBuilder;
import qiwi.testapp.builder.ui.ValidatorStrategy;

public class UIBuilderTest {

    UIFormat test;
    UIBuilder manager;

    @Before
    public void lineUP(){
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        test = gson.fromJson(ExampleUnitTest.Payload.fromFile, UIFormat.class);
    }


    @Test
    public void createTreeTest() throws IOException {

    }


    @Test
    public void UIDependencyTest() throws IOException {

        Element masterField = test.content.elements.get(0);
        Element slaveField  = test.content.elements.get(1);

        PublishSubject<String> o = PublishSubject.create();
        io.reactivex.functions.Function<? super String, Boolean> masterFunc =
                ValidatorStrategy.createValidator(masterField.validator);
        io.reactivex.functions.Function<? super String, Boolean> slaveFunc  =
                ValidatorStrategy.createCondition(slaveField.condition);
        o.subscribe(next -> {
            boolean b = masterFunc.apply(next);
            System.out.print(b);
            System.out.println("-from master");
        });

        Observable<String> depO = o;

        depO.subscribe(next -> {
            boolean b = slaveFunc.apply(next);
            System.out.print(b);
            System.out.println("-from slave");
        });

        o.onNext("2");
        o.onNext("5");
        o.onNext("-1");



    }


}
