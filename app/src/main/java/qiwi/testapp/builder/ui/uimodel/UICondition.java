package qiwi.testapp.builder.ui.uimodel;


import android.view.ViewGroup;

import io.reactivex.Observable;
import qiwi.testapp.builder.shemamodel.Condition;

public class UICondition extends UIVerifier {

    Condition condition;
    ViewGroup viewGroup;

    public UICondition(UIElement dependency,
                       Observable<CharSequence> observable) {

        super(dependency, observable);
        condition = uiElement.element.condition;
        viewGroup = ((UIDependency) dependency).viewGroup;
    }

    public Condition getCondition() {
        return condition;
    }

    public ViewGroup getViewGroup() {
        return viewGroup;
    }

    @Override
    public int hashCode() {
        return  getUiElement().hashCode();
    }
}
