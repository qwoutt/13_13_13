package qiwi.testapp.builder.ui;

import android.view.ViewGroup;

import com.annimon.stream.Stream;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

import io.reactivex.Observable;
import qiwi.testapp.builder.shemamodel.Content;
import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.ui.uimodel.UIContent;
import qiwi.testapp.builder.ui.uimodel.UIDependency;
import qiwi.testapp.builder.ui.uimodel.UIElement;
import qiwi.testapp.builder.ui.uimodel.UIField;
import qiwi.testapp.builder.ui.uimodel.UICondition;
import qiwi.testapp.builder.ui.uimodel.UIObserver;
import qiwi.testapp.builder.ui.uimodel.UIValidator;

public class UIBuilder {

    private Content content;
    private UIContent uiContent = new UIContent();
    private ViewGroup mainLayout;

    private HashSet<UIField>     fields       = new HashSet<>();
    private HashSet<UIObserver>  observers    = new HashSet<>();
    private HashSet<UIValidator> validators   = new HashSet<>();
    private HashSet<UICondition> conditions = new HashSet<>();

    public UIBuilder(Content content, ViewGroup viewGroup) throws IOException {
       this.content = content;
       this.mainLayout = viewGroup;
    }


    public UIBuilder buildUITree(){
        for (Element e : content.elements){
            uiContent.elements.add(ElementStrategy.createUIElement(e));
        }
        return this;
    }

    public UIBuilder renderUITree(){
        for (UIElement e : uiContent.elements){
            ElementStrategy.renderView(mainLayout, e);
        }
        return this;
    }

    public UIBuilder buildEvents(){
        return buildEvents(uiContent.elements);
    }

    UIBuilder buildEvents(List<UIElement> uiElements){
        for (UIElement e : uiElements){
            if(e instanceof UIField) {
                UIField uiField = (UIField)e;
                Observable<CharSequence> o = WidgetStrategy.createObservableForValidator(uiField);
                UIObserver uiObserver = new UIObserver(uiField, o);
                fields.add(uiField);
                observers.add(uiObserver);
                validators.add(new UIValidator(uiField, uiObserver.getObservable()));
            }else{
                UIDependency dependency = (UIDependency)e;

                buildEvents(dependency.getContent().elements);
            }
        }
        return this;
    }

    public UIBuilder buildDependencies(){return buildDependencies(uiContent.elements);}

    UIBuilder buildDependencies(List<UIElement> uiElements){
        for (UIElement e : uiElements){
            if(e instanceof UIDependency) {
                UIDependency dependency = (UIDependency)e;
                UIObserver uiObserver = findUIObserver(dependency.getElement().condition.field);
                Observable<CharSequence> o = uiObserver.getObservable();
                conditions.add(new UICondition(dependency, o));

                buildDependencies(dependency.getContent().elements);
            }
        }
        return this;
    }

    public ViewGroup getMainLayout() {
        return mainLayout;
    }

    public HashSet<UIValidator> getValidators() {
        return validators;
    }

    public HashSet<UICondition> getConditions() {
        return conditions;
    }

    public HashSet<UIObserver> getObservers() {
        return observers;
    }

    public HashSet<UIField> getFields() {
        return fields;
    }

    private UIObserver findUIObserver(String fieldName){
        return Stream.of(observers)
                .filter(o -> o.getFieldName().equals(fieldName))
                .findFirst()
                .get();
    }
}
