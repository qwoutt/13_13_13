package qiwi.testapp.builder.ui.uimodel;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;

public class UIObserver {

    UIField uiField;
    Observable<CharSequence> o;
    Disposable disposable;
    final List<PublishSubject<CharSequence>> subscriberList = new ArrayList<>();

    public UIObserver(UIField uiField, Observable<CharSequence> o) {
        this.uiField = uiField;
        this.o = o;
    }

    public void bind(){
        disposable = o.observeOn(AndroidSchedulers.mainThread())
                .subscribe(next -> {
                    for (PublishSubject<CharSequence> sub :  subscriberList){
                        sub.onNext(next);
                    }
                });
    }

    public Observable<CharSequence> getObservable(){
        PublishSubject<CharSequence> newSub = PublishSubject.create();
        subscriberList.add(newSub);
        return newSub;
    }

    public void dispose(){
        if(disposable.isDisposed()){
            disposable.dispose();
        }
    }

    public String getFieldName(){
        return uiField.element.name;
    }
}
