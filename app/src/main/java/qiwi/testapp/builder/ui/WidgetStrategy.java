package qiwi.testapp.builder.ui;

import android.text.InputType;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import com.jakewharton.rxbinding2.widget.RxAdapterView;
import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import qiwi.testapp.builder.custom.CustomDropDownAdapter;
import qiwi.testapp.builder.shemamodel.Choice;
import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.ui.uimodel.UIField;

public enum WidgetStrategy {

    radio {
        @Override
        Spinner createWidgetOnType(ViewGroup view, Element element) {
            CustomDropDownAdapter adapter = WidgetStrategy.radio.createAdapter(view, element.view);
            Spinner spinner = new Spinner(view.getContext());
            spinner.setAdapter(adapter);
            spinner.setId(ElementStrategy.getID(element));
            return spinner;
        }

        @Override
        public Observable<String> createObservable(UIField field) {
            Spinner spinner = (Spinner) field.view;
            return RxAdapterView.itemSelections(spinner)
                    .skip(2)
                    .debounce(radio_timeout, TimeUnit.MILLISECONDS)
                    .map(i -> ((Choice)spinner.getItemAtPosition(i)).value);
        }
    },

    text {
        @Override
        EditText createWidgetOnType(ViewGroup view, Element element) {
            EditText editText = new EditText(view.getContext());
            editText.setId(ElementStrategy.getID(element));
            editText.setHint(element.view.prompt);

            if(element.view.widget.keyboard != null) {
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
            }else {
                editText.setInputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS);
            }

            return editText;
        }

        @Override
        public Observable<CharSequence> createObservable(UIField field) {
            EditText editText = (EditText) field.view;
            return RxTextView.textChanges(editText).skipInitialValue()
                    .debounce(text_timeout, TimeUnit.MILLISECONDS);
        }
    },

    title{
        @Override
        TextView createWidgetOnType(ViewGroup view, Element element) {
            TextView text = new TextView(view.getContext());
            text.setTextSize(TypedValue.COMPLEX_UNIT_SP, title_size);
            text.setText(element.view.title);
            return text;
        }

        @Override
        public Observable<CharSequence> createObservable(UIField field) {;
            TextView textView = (TextView) field.view;
            return RxTextView.textChanges(textView);
        }
    };

    private final static int title_size = 10;
    private final static String prompt_adapter_id = "-1";
    private final static int radio_timeout = 200;
    private final static int text_timeout = 1000;

    abstract <T extends View> T createWidgetOnType(ViewGroup view, Element element);

    public abstract <T extends View> Observable<CharSequence> createObservable(UIField field);

    public static View createWidget(ViewGroup view, Element element){
        if(element.view != null){
            TextView textView = WidgetStrategy.title.createWidgetOnType(view , element);
            LayoutFactory.addToViewGroup(view, textView);
        }

        View newView = WidgetStrategy
                .valueOf(element.view.widget.type)
                .createWidgetOnType(view, element);
        LayoutFactory.addToViewGroup(view, newView);
        return newView;
    }

    private CustomDropDownAdapter createAdapter(View view,
                                                qiwi.testapp.builder.shemamodel.View viewInfo){
        CustomDropDownAdapter adapter = new CustomDropDownAdapter(view.getContext());

        Choice prompt = new Choice();
        prompt.title = viewInfo.prompt;
        prompt.value = prompt_adapter_id;

        adapter.add(prompt);
        for (Choice choice : viewInfo.widget.choices) {
            adapter.add(choice);
        }
        return adapter;
    }

    public static Observable<CharSequence> createObservableForValidator(UIField field){
        String wType = field.getElement().view.widget.type;
        Observable<CharSequence> o = WidgetStrategy.valueOf(wType).createObservable(field);
        return o;
    }

}
