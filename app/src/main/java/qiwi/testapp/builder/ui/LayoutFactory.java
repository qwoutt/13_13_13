package qiwi.testapp.builder.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class LayoutFactory {

    private static final int layout_width  = LinearLayout.LayoutParams.MATCH_PARENT;
    private static final int layout_height = LinearLayout.LayoutParams.WRAP_CONTENT;

    public static ViewGroup addToViewGroup(ViewGroup layout, View view){
        addView(layout,view);
        return layout;
    }

    public static LinearLayout createLayout(Context context){
        LinearLayout linearLayout = new LinearLayout(context);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setVisibility(View.GONE);
        return linearLayout;
    }


    private static void addView(ViewGroup layout, View view){
        layout.addView(
                view,
                layout_width,
                layout_height
        );
    }


}
