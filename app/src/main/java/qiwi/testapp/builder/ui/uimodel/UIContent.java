package qiwi.testapp.builder.ui.uimodel;


import java.util.ArrayList;
import java.util.List;

public class UIContent {
    public List<UIElement> elements = new ArrayList<>();

    public void addUIElement(UIElement e){
        elements.add(e);
    }
}
