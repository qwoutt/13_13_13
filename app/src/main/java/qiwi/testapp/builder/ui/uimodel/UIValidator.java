package qiwi.testapp.builder.ui.uimodel;


import io.reactivex.Observable;
import qiwi.testapp.builder.shemamodel.Validator;

public class UIValidator extends UIVerifier {

    String message;
    Validator validator;

    public UIValidator(UIElement uiElement, Observable<CharSequence> observable) {
        super(uiElement, observable);
        message = uiElement.element.validator.message;
        validator = uiElement.element.validator;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Validator getValidator() {
        return validator;
    }

    public void setValidator(Validator validator) {
        this.validator = validator;
    }

    public String getFieldName(){
        return uiElement.element.name;
    }

    public UIField getFiled(){
        return (UIField)getUiElement();
    }

}
