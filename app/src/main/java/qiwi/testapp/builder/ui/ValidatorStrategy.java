package qiwi.testapp.builder.ui;


import java.util.regex.Matcher;
import java.util.regex.Pattern;
import io.reactivex.functions.Function;
import qiwi.testapp.builder.shemamodel.Condition;
import qiwi.testapp.builder.shemamodel.Validator;
import qiwi.testapp.builder.utils.BindingUtils;

public enum ValidatorStrategy {

    predicate{
        @Override
        public boolean validate(Validator validator, CharSequence o) {
            return ValidatorStrategy.valueOf(validator.predicate.type).validate(validator, o);
        }
    },

    regex{
        @Override
        public boolean validate(Validator validator, CharSequence o) {
            Pattern p = Pattern.compile(validator.predicate.pattern);
            Matcher m = p.matcher(o);
            return m.matches();
        }
    };

    public abstract boolean validate(Validator validator, CharSequence o);


    public static Function<? super CharSequence, Boolean> createValidator(Validator validator){
        Function<? super CharSequence, Boolean> rxValidator = s-> valueOf(validator.type)
                .validate(validator, s);
        return rxValidator;
    }


    public static Function<? super CharSequence, Boolean> createCondition(Condition condition){
        Validator validator = BindingUtils.getValidatorFromCondition(condition);
        return createValidator(validator);
    }

}
