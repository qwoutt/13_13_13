package qiwi.testapp.builder.ui.uimodel;

import android.support.annotation.VisibleForTesting;

import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.ui.ElementStrategy;


public abstract class UIElement {

    Element element;
    int id;

    @Override
    public int hashCode() {
        return getID();
    }

    @Override
    public boolean equals(Object obj) {

        if(obj instanceof UIElement){
            UIElement e = (UIElement)obj;
            if(e.hashCode() == this.hashCode()){
                return true;
            }

        }

        return false;
    }

    private int getID(){
        if(id == 0){
            id = ElementStrategy.getID(element);
            return id;
        }else {
            return id;
        }
    }

    @VisibleForTesting
    private void setId(int id) {
        this.id = id;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }
}
