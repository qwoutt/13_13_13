package qiwi.testapp.builder.ui.uimodel;

import io.reactivex.Observable;

public abstract class UIVerifier {

    UIElement uiElement;

    Observable<CharSequence> observable;

    public Observable<CharSequence> getObservable() {
        return observable;
    }

    public UIVerifier(UIElement uiElement, Observable<CharSequence> observable) {
        this.uiElement = uiElement;
        this.observable = observable;
    }

    protected UIElement getUiElement() {
        return uiElement;
    }
}
