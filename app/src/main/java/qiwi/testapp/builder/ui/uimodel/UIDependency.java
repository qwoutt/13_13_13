package qiwi.testapp.builder.ui.uimodel;

import android.view.ViewGroup;

import qiwi.testapp.builder.shemamodel.Condition;

public class UIDependency extends UIElement{

    ViewGroup viewGroup;
    UIContent content = new UIContent();

    public void addUIElement(UIElement e){content.addUIElement(e);}

    public void setViewGroup(ViewGroup viewGroup){
        this.viewGroup = viewGroup;
    }

    public ViewGroup getViewGroup() {
        return viewGroup;
    }

    public UIContent getContent() {
        return content;
    }



}
