package qiwi.testapp.builder.ui;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.shemamodel.Validator;
import qiwi.testapp.builder.ui.uimodel.UIDependency;
import qiwi.testapp.builder.ui.uimodel.UIElement;
import qiwi.testapp.builder.ui.uimodel.UIField;
import qiwi.testapp.builder.utils.BindingUtils;

public enum ElementStrategy{

    field {
        @Override
        void createView(ViewGroup layout, UIElement uiElement) {
          View view = WidgetStrategy.createWidget(layout, uiElement.getElement());
          ((UIField)uiElement).view = view;
        }

        @Override
        public int scoreID(Element element) {
            if(element.semantics == null){
                return element.name.hashCode();
            }else {
                return element.semantics.type.hashCode();
            }
        }

        @Override
        public boolean buildValidator(Element element,String s) {
            return ValidatorStrategy.valueOf(element.validator.type)
                    .validate(element.validator, s);
        }

        @Override
        UIElement createUIElementOnType(Element element) {
            UIField uiField = new UIField();
            uiField.setElement(element);
            return uiField;
        }
    },

    dependency {
        @Override
        void createView(ViewGroup layout, UIElement uiElement) {
            UIDependency uiDependency = (UIDependency) uiElement;
            LinearLayout linearLayout = LayoutFactory.createLayout(layout.getContext());
            uiDependency.setViewGroup(linearLayout);

            for (UIElement uiElement1 : uiDependency.getContent().elements) {
                View newView =  WidgetStrategy.createWidget(linearLayout, uiElement1.getElement());
                ((UIField)uiElement1).view = newView;
            }

            LayoutFactory.addToViewGroup(layout, linearLayout);
        }

        public int scoreID(Element element){
            int hash = 0;
            for (Element el : element.content.elements) {
                hash += ElementStrategy.valueOf(el.type).getID(el);
            }
            return hash;
        }

        @Override
        public boolean buildValidator(Element element,String s) {
            Validator validator = BindingUtils.getValidatorFromCondition(element.condition);
            return ValidatorStrategy.valueOf(validator.type).validate(validator, s);
        }

        @Override
        UIElement createUIElementOnType(Element element) {
            UIDependency uiDependency = new UIDependency();
            uiDependency.setElement(element);

            for (Element e : element.content.elements){
                uiDependency.addUIElement(createUIElement(e));
            }

            return uiDependency;
        }
    };

    abstract void createView(ViewGroup layout, UIElement uiElement);

    public static void renderView(ViewGroup layout, UIElement uiElement){
        ElementStrategy.valueOf(uiElement.getElement().type)
                .createView(layout, uiElement);
    }

    abstract int scoreID(Element element);

    abstract boolean buildValidator(Element element, String s);

    public static int getID(Element element) {
        return ElementStrategy.valueOf(element.type).scoreID(element);
    }

    abstract UIElement createUIElementOnType(Element element);

    public static UIElement createUIElement(Element element){
        return ElementStrategy.valueOf(element.type).createUIElementOnType(element);
    }
}
