package qiwi.testapp.builder.custom;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import qiwi.testapp.R;
import qiwi.testapp.builder.shemamodel.Choice;

public class CustomDropDownAdapter extends ArrayAdapter<Choice> {

    public CustomDropDownAdapter(@NonNull Context context) {
        super(context, R.layout.support_simple_spinner_dropdown_item);
    }

    @Override
    public boolean isEnabled(int position){
        return position != 0;
    }
    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        View view = super.getDropDownView(position, convertView, parent);
        TextView tv = (TextView) view;
        tv.setText(getTitle(position));
        if(position == 0){
            tv.setTextColor(Color.GRAY);
        }
        else {
            tv.setTextColor(Color.BLACK);
        }
        return view;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        TextView tv = (TextView) super.getView(position, convertView, parent);
        tv.setText(getTitle(position));
        return tv;
    }

    private String getTitle(int i){
        return getItem(i).title;
    }
}
