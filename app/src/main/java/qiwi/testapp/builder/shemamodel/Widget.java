package qiwi.testapp.builder.shemamodel;

import java.util.List;

public class Widget{

    public String type;
    public String keyboard;
    public List<Choice> choices = null;

}
