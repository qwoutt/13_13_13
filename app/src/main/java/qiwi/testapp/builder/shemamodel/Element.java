package qiwi.testapp.builder.shemamodel;


public class Element {

    public String type;
    public String name;
    public Validator validator;
    public View view;
    public Condition condition;
    public Content content;
    public Semantics semantics;

}


