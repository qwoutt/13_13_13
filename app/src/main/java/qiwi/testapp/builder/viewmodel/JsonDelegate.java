package qiwi.testapp.builder.viewmodel;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;

import qiwi.testapp.builder.shemamodel.Content;
import qiwi.testapp.builder.shemamodel.Element;
import qiwi.testapp.builder.shemamodel.UIFormat;
import qiwi.testapp.builder.ui.ElementStrategy;

public class JsonDelegate {

    private UIFormat format;
    private Content content;
    public JsonDelegate(String json) {
        Gson gson = new Gson();
        format = gson.fromJson(json, UIFormat.class);
        content = format.content;
    }

    public Content getContent() {
        return content;
    }
}
