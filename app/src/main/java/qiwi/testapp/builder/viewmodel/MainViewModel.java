package qiwi.testapp.builder.viewmodel;

import android.arch.lifecycle.ViewModel;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;
import qiwi.testapp.builder.shemamodel.Content;
import qiwi.testapp.builder.ui.LayoutFactory;
import qiwi.testapp.builder.ui.UIBuilder;
import qiwi.testapp.builder.ui.ValidatorStrategy;
import qiwi.testapp.builder.ui.uimodel.UICondition;
import qiwi.testapp.builder.ui.uimodel.UIField;
import qiwi.testapp.builder.ui.uimodel.UIObserver;
import qiwi.testapp.builder.ui.uimodel.UIValidator;
import qiwi.testapp.viewstate.ViewState;

public class MainViewModel extends ViewModel {

    Content content;
    UIBuilder builder;

    private InputStreamReader streamReader;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private PublishSubject<ViewState.DependencyState> dependencyStatePublishSubject = PublishSubject.create();
    private PublishSubject<ViewState.InputState> inputStatePublishSubject = PublishSubject.create();

    private ViewState.RenderState lastRenderState;

    public MainViewModel(@NonNull InputStreamReader streamReader) {
        this.streamReader = streamReader;
    }

    private String readUIJsonFile(InputStreamReader streamReader) throws IOException{
            StringBuilder json = new StringBuilder();
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(streamReader);
                String mLine;
                while ((mLine = reader.readLine()) != null) {
                    json.append(mLine);
                }
            } catch (IOException e) {

            } finally {
                if (reader != null) {
                    reader.close();
                }
            }
            return json.toString();
    }


    @Override
    protected void onCleared() {
        super.onCleared();
        disposeUIObservers();
        compositeDisposable.clear();
    }

    private void subscribeUIFields() {
        try {
            for (UIObserver o : builder.getObservers()) {
                o.bind();
            }
        }catch (NullPointerException npe){
            npe.printStackTrace();
            Log.e("NPE_BIND", npe.getLocalizedMessage());
        }
    }

    private void disposeUIObservers() {
        try {
            for (UIObserver o : builder.getObservers()) {
                o.dispose();
            }
        }catch (NullPointerException npe){
            npe.printStackTrace();
            Log.e("NPE_BIND", npe.getLocalizedMessage());
        }
    }

    public Single<ViewState.RenderState> showUI(Context c){
        ViewGroup viewGroup = LayoutFactory.createLayout(c);

        if(lastRenderState == null) {

            try {
                return Single.just(readUIJsonFile(streamReader))
                        .map(json -> {
                            JsonDelegate delegate = new JsonDelegate(json);
                            this.content = delegate.getContent();
                            builder = new UIBuilder(content, viewGroup)
                                    .buildUITree()
                                    .renderUITree()
                                    .buildEvents()
                                    .buildDependencies();

                            subscribeUIFields();
                            lastRenderState = new ViewState.RenderState.RenderSuccess(builder.getMainLayout());
                            return lastRenderState;
                        });
            } catch (IOException e) {
                e.printStackTrace();
                return Single.just(new ViewState.RenderState.RenderError());
            }
        }else {
            return Single.just(lastRenderState);
        }
    }


    public Observable<ViewState.InputState> observableInput() {
        if(builder.getValidators() != null){
            for (UIValidator uiValidator : builder.getValidators()) {
                UIField uiField = uiValidator.getFiled();
                Disposable d = uiValidator.getObservable()
                        .map(ValidatorStrategy.createValidator(uiValidator.getValidator()))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(isValid -> {
                                    ViewState.InputState state;
                                    if (isValid) {
                                        state = new ViewState.InputState.InputRight(uiField);
                                    } else {
                                        state = new ViewState.InputState.ShowError(uiField, uiValidator.getMessage());
                                    }
                                    inputStatePublishSubject.onNext(state);
                                }
                        );

                compositeDisposable.add(d);
            }
        }

        return inputStatePublishSubject;
    }

    public Observable<ViewState.DependencyState> observableDependencies() {
        if(builder.getConditions() != null){
            for (UICondition uiCondition : builder.getConditions()) {
                ViewGroup viewGroup = uiCondition.getViewGroup();
                Disposable d = uiCondition.getObservable()
                        .map(ValidatorStrategy.createCondition(uiCondition.getCondition()))
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(isValid -> {
                                    ViewState.DependencyState state;
                                    if (isValid) {
                                        state = new ViewState.DependencyState.ShowType(viewGroup);
                                    } else {
                                        state = new ViewState.DependencyState.HideType(viewGroup);
                                    }
                                    dependencyStatePublishSubject.onNext(state);
                                }
                        );

                compositeDisposable.add(d);
            }
        }

        return dependencyStatePublishSubject;
    }


}
