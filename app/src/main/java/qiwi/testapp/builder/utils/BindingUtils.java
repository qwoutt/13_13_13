package qiwi.testapp.builder.utils;


import qiwi.testapp.builder.shemamodel.Condition;
import qiwi.testapp.builder.shemamodel.Validator;

public class BindingUtils {
    public static Validator getValidatorFromCondition(Condition condition){
        Validator validator = new Validator();
        validator.message   = null;
        validator.type      = condition.type;
        validator.predicate = condition.predicate;
        return validator;
    }
}
