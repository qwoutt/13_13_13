package qiwi.testapp.builder.utils;


import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStreamReader;

import qiwi.testapp.MainActivity;
import qiwi.testapp.R;
import qiwi.testapp.builder.viewmodel.MainViewModel;

public class ViewModelFactory {

    private static final String utf_8 = "UTF-8";

    public static MainViewModel fetchMainVM(MainActivity activity, String filename){
       return ViewModelProviders.of(activity, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                try {
                    return (T) new MainViewModel(
                            new InputStreamReader(activity.getAssets().open(filename), utf_8));
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }).get(MainViewModel.class);
    }
}
