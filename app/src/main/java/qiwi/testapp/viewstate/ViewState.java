package qiwi.testapp.viewstate;

import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import qiwi.testapp.builder.ui.uimodel.UIField;

public interface ViewState {

    public interface RenderState extends ViewState{

        public class RenderNotYet  implements RenderState{}

        public class RenderError   implements RenderState{

            final String error_message = "Ошибка! \n" +
                    "Невозможно отрисовать форму";

            public String getMessage(){
                return error_message;
            }

        }

        public class RenderSuccess implements RenderState{

            ViewGroup viewGroup;

            public RenderSuccess(ViewGroup viewGroup) {
                this.viewGroup = viewGroup;
            }

            public ViewGroup getViewGroup() {
                return viewGroup;
            }
        }
    }

    public abstract class InputState implements ViewState{

        public final UIField field;

        public InputState(UIField field) {
            this.field = field;
        }

        public static class ShowError extends InputState{

            String message;

            public ShowError(UIField field, String message) {
                super(field);
                this.message = message;
            }

            public String getMessage() {
                return message;
            }
        }

        public static class InputRight extends InputState {

            public InputRight(UIField field) {
                super(field);
            }

        }

        public static class Empty extends InputState{

            public Empty(UIField field) {
                super(field);
            }
        }

    }


    public abstract class DependencyState implements ViewState{

        ViewGroup viewGroup;

        public static class NotChosen implements ViewState{
            public NotChosen(){};
        }

        public static class ShowType extends DependencyState{
            public ShowType(ViewGroup viewGroup) {
                super(viewGroup);
            }
        }

        public static class HideType extends DependencyState{
            public HideType(ViewGroup viewGroup) {
                super(viewGroup);
            }
        }

        public DependencyState(ViewGroup viewGroup) {
            this.viewGroup = viewGroup;
        }

        public ViewGroup getViewGroup() {
            return viewGroup;
        }
    }


    public class CommonState implements ViewState{

            RenderState renderState;
            InputState  inputState;
            DependencyState dependencyState;

            public CommonState(RenderState renderState,
                               InputState inputState,
                               DependencyState dependencyState
            ) {
                this.renderState = renderState;
                this.inputState = inputState;
                this.dependencyState = dependencyState;
            }

            public RenderState getRenderState() {
                return renderState;
            }

            public InputState getInputState() {
                return inputState;
            }

            public DependencyState getDependencyState() {
                    return dependencyState;
            }
    }

}
