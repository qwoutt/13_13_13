package qiwi.testapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.HashMap;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import qiwi.testapp.builder.utils.ViewModelFactory;
import qiwi.testapp.builder.viewmodel.MainViewModel;
import qiwi.testapp.viewstate.ViewState;

public class MainActivity extends AppCompatActivity{

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    private MainViewModel viewModel;
    private LinearLayout contentLayout;

    private ViewGroup contentFromVM;

    private HashMap<View,Boolean> fieldsStateMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        contentLayout = findViewById(R.id.contentLayout);
        viewModel = ViewModelFactory.fetchMainVM(this,
                getString(R.string.json_path));
        bindViewModel();
    }


    private void bindViewModel(){
        viewModel.showUI(this)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::renderState);
    }

    private void renderState(ViewState.RenderState state){

        if(state instanceof ViewState.RenderState.RenderError){
            showToast(((ViewState.RenderState.RenderError) state).getMessage());
        }

        if(state instanceof ViewState.RenderState.RenderSuccess){

            ViewGroup viewGroup = ((ViewState.RenderState.RenderSuccess) state).getViewGroup();
            contentFromVM = viewGroup;
            viewGroup.setVisibility(View.VISIBLE);
            contentLayout.addView(viewGroup);
        }

    }

    private void renderState(ViewState.InputState state){

        if(state instanceof ViewState.InputState.InputRight) {
            fieldsStateMap.put(state.field.view, true);
        }

        if(state instanceof ViewState.InputState.ShowError){
            showToast(((ViewState.InputState.ShowError) state).getMessage());
        }
    }

    private void renderState(ViewState.DependencyState state){

        if(state instanceof ViewState.DependencyState.ShowType){
            state.getViewGroup().setVisibility(View.VISIBLE);

        }

        if(state instanceof ViewState.DependencyState.HideType){
            state.getViewGroup().setVisibility(android.view.View.GONE);
        }
    }

    private void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void subscribeOnViewModel(MainViewModel vm){
        compositeDisposable.add(vm.observableInput().subscribe(this::renderState));
        compositeDisposable.add(vm.observableDependencies().subscribe(this::renderState));
    }

    @Override
    protected void onResume() {
        super.onResume();
        subscribeOnViewModel(viewModel);
    }

    private void unsubscribeOnViewModels(){
        compositeDisposable.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unsubscribeOnViewModels();
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        contentLayout.removeView(contentFromVM);
    }
}
